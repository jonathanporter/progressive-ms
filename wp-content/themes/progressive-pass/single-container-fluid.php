<div class="">   
<?php
/*
 * Template Name: Single Post - 'container-fluid'
 * Template Post Type: post
 */
 
get_header();  ?>

</div>
<div class="wrapper" >
	<div class="row">
		<div class="container-fluid" id="content" tabindex="-1">



			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

						<?php understrap_post_nav(); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- Container end -->
	</div><!-- .row -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
