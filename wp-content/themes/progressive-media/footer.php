<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="contact-footer pb-5 mt-5 mask-group-tl">
    <div class="container">
        <div class="row">
            <div class="col-md-6 pl-4 pr-4">
              <div class="row-before-arrow-3-down-top">
                <h3 data-aos="fade" class="mt-4 aos-init aos-animate" id="signup">Keep up to date</h3>
                  <p>Get the latest property news and techniques with our exclusive Property Insider newsletter, direct to your inbox.</p>
              </div>
            </div>
            <div class="col-md-6 pl-4 pr-4">
                <?php gravity_form(4, false, false, false, '', true, 12); ?>
            </div>
        </div>
    </div>
</div>
<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info pt-3 pb-3">
						&copy; 2019 <a class="inherit-color" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a> <a href="//beprogressive.com/">Progressive Group</a><span class="sep"> | </span>Digital: <a href="//beprogressive.com/">Progressive Design</a>
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script src="/wp-content/themes/progressive-property/js/brand-nav-toggler.js"></script>
<script src="/wp-content/themes/progressive-property/js/change-nav-on-scroll.js"></script>
<script src="/wp-content/themes/progressive-property/js/tick-animation.js"></script>
<script src="/wp-content/themes/progressive-property/js/counter-count.js"></script>
<script src="/wp-content/themes/progressive-property/js/jquery.ticker.min.js"></script>
<script src="/wp-content/themes/progressive-property/js/aos.js"></script>

<script>
  AOS.init({
  offset: 0,
  easing: 'ease-in-quad',
  mirror: true
  });
</script>

<script type="text/javascript">
  document.querySelectorAll(".*-bg-row")
  .forEach(node => node.setAttribute("data-aos", "fade"));

  AOS.init(); // or `AOS.refresh();` if it is initialized
</script>

<script>
jQuery.noConflict();
 (function( $ ) {
    // Your jQuery code here, using the $
    $('.ticker').ticker();
})( jQuery );
</script>

<script>
jQuery(document).ready(function($) {
    $(".clickable-div").click(function() {
  window.location = $(this).find("a").attr("href"); 
  return false; });
});
</script>
</body>
</html>

