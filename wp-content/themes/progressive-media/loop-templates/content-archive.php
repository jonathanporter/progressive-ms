<?php
/**
 * Partial template for content in archive?.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<div class="card card-post-grid m-3" style="min-width: 25%;">
<article id="post-<?php the_ID(); ?>">
	<div class="card-header card-img-top p-0">

<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
	<header class="entry-header negative tinted-image" style="
		background-image: linear-gradient(
		rgba(0, 128, 198, 1), 
		rgba(62, 47, 128, 1)
		), url('<?php echo $backgroundImg[0]; ?>');">
			<?php the_title( '<h1 class="h4">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	</div>
	<div class="card-body small" >
		<?php the_excerpt(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
</div>

