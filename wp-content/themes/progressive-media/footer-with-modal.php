<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="contact-footer pb-5 mask-group-tl">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
              <div class="col">
                <h3>Keep up to date</h3>
                <p>Get the latest property news and techniques with our exclusive Property Insider newsletter, direct to your inbox.</p>
              </div>
            </div>
            <div class="col-md-6 pl-4 pr-4">
                <?php gravity_form(4, false, false, false, '', true, 12); ?>
            </div>
        </div>
    </div>
</div>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info pt-3 pb-3">

						&copy; 2018 <a class="inherit-color" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a> <a href="//progressive.test/">Progressive Group</a><span class="sep"> | </span>Design: <a href="//progressive.test/">Progressive Digital</a>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script src="/wp-content/themes/progressive-property/js/brand-nav-toggler.js"></script>
<script src="/wp-content/themes/progressive-property/js/change-nav-on-scroll.js"></script>
<script src="/wp-content/themes/progressive-property/js/tick-animation.js"></script>
<script src="/wp-content/themes/progressive-property/js/counter-count.js"></script>
<script src="/wp-content/themes/progressive-group/js/jquery.ticker.min.js"></script>
<script src="/wp-content/themes/progressive-property/js/aos.js"></script>
<script>
  AOS.init();
</script>

<script>
jQuery.noConflict();
 (function( $ ) {
    // Your jQuery code here, using the $
    $('.ticker').ticker();
})( jQuery );
</script>

<script>
jQuery(document).ready(function($) {
    $(".clickable-div").click(function() {
  window.location = $(this).find("a").attr("href"); 
  return false; });
});
</script>
<!-- <script type="text/javascript">
  modalContent.load(post_link + ' #modal-ready');

</script> -->

<!-- Registration Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="registrationModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="container">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      
      </div>
      </div>
    </div>
  </div>
</div><!-- END Registration Modal -->
</body>
</html>

