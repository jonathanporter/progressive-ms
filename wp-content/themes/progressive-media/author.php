<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<?php $backgroundImg = get_avatar_data( $id_or_email, $args ); ?>
<div class="wrapper" id="author-wrapper">



			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<!-- <header class="page-header author-header"> -->

				<header class="entry-header negative tinted-image" style="
						background-image: linear-gradient(
						rgba(0, 128, 198, 1), 
						rgba(62, 47, 128, 1)
						), url('<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>');">	

					<?php
					if ( isset( $_GET['author_name'] ) ) {
						$curauth = get_user_by( 'slug', $author_name );
					} else {
						$curauth = get_userdata( intval( $author ) );
					}
					?>
					<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
						<h1><?php echo esc_html__( 'About:', 'understrap' ) . ' ' . esc_html( $curauth->nickname ); ?></h1>
					</div>
				</header><!-- .page-heade	r -->
				
				<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="d-flex">


						<div class="col-3 img-fluid mt-3">
							<?php if ( ! empty( $curauth->ID ) ) : ?>
								<?php echo get_avatar( $curauth->ID, 256); ?>
							<?php endif; ?>
						</div>

							<?php if ( ! empty( $curauth->user_url ) || ! empty( $curauth->user_description ) ) : ?>

						<dl class=" pt-5 pb-5">
							<?php if ( ! empty( $curauth->user_url ) ) : ?>
								<dt><?php esc_html_e( 'Website', 'understrap' ); ?></dt>
								<dd>
									<a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo esc_html( $curauth->user_url ); ?></a>
								</dd>
							<?php endif; ?>

							<?php if ( ! empty( $curauth->user_description ) ) : ?>
								<dt><?php esc_html_e( 'Profile', 'understrap' ); ?></dt>
								<dd class="lead"><?php esc_html_e( $curauth->user_description ); ?></dd>
							<?php endif; ?>
						</dl>
							<?php endif; ?>
		</div>
					<h3><?php echo esc_html( 'Posts by', 'understrap' ) . ' ' . esc_html( $curauth->nickname ); ?>:</h2>




				<ul class="author-posts">
					<!-- The Loop -->
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<li>
								<?php
								printf(
									'<a rel="bookmark" href="%1$s" title="%2$s %3$s">%3$s</a>',
									esc_url( apply_filters( 'the_permalink', get_permalink( $post ), $post ) ),
									esc_attr( __( 'Permanent Link:', 'understrap' ) ),
									the_title( '', '', false )
								);
								?>
								<?php understrap_posted_on(); ?> 
								<?php esc_html_e( 'in', 'understrap' ); ?> 
								<?php the_category( ' & ' ); ?>
							</li>
						<?php endwhile; ?>

					<?php else : ?>

						<?php get_template_part( 'loop-templates/content', 'none' ); ?>

					<?php endif; ?>

					<!-- End Loop -->

				</ul>

			</main><!-- #main -->


			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

	</div><!-- #content -->

</div><!-- #author-wrapper -->
	<div class="container">
		<!-- The pagination component -->
		<?php understrap_pagination(); ?>
	</div>
<?php get_footer(); ?>
