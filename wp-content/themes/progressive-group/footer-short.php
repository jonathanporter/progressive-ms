<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>


</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script src="/wp-content/themes/progressive-property/js/brand-nav-toggler.js"></script>
<script src="/wp-content/themes/progressive-group/js/change-nav-on-scroll.js"></script>
<script src="/wp-content/themes/progressive-property/js/tick-animation.js"></script>

<script src="/wp-content/themes/progressive-group/js/prefixfree.min.js"></script>
 <script src="/wp-content/themes/progressive-group/js/aos.js"></script>
  <script>
    AOS.init({
    offset: 0,
    easing: 'ease-in-quad',
    mirror: true
    });
  </script>
  <script src="/wp-content/themes/progressive-group/js/jquery.ticker.min.js"></script>
<script>
jQuery.noConflict();
 (function( $ ) {
    $('.ticker').ticker();
})( jQuery );
</script>

<script>
jQuery(document).ready(function($) {
    $(".clickable-div").click(function() {
  window.location = $(this).find("a").attr("href"); 
  return false;
  });
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($) {
      $('.js-conveyor').jConveyorTicker({
        anim_duration: 200
      });
    });
</script>
<script src="/wp-content/themes/progressive-group/js/jquery.jConveyorTicker.min.js"></script>
</body>
</html>
