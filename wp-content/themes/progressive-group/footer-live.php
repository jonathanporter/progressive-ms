<?php
/**
 * The template for displaying the Live Stream footer inc JS.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>


</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script type="text/javascript">
  var linksThatOpenInANewWindow = {
    'https://bit.ly/bbstickets': 1
};

onOpen: function(e, i) {
    if (linksThatOpenInANewWindow[e] === 1) {
        window.open(e);   
    } else {
        location = e
    }
}
</script>
</body>
</html>
