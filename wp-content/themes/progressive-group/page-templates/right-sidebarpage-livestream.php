<?php
/**
 * Template Name: Live Stream Right Sidebar Comments
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */

get_header('blank');
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="page-wrapper">

	<div id="p-comments-wraper" class="container-fluid" id="content">

		<div class="row">

			<div class="<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>col-lg-8<?php else : ?>col-md-12<?php endif; ?> content-area"
				id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'empty' ); ?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<div id="p-comments-container" class="gray-lighter-bg p-3 col-lg-4">

				<h3 class="pl-3">Live Chat</h3>
				<?php if(function_exists('display_saic')) { echo display_saic();} ?>


			</div>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer('live'); ?>
