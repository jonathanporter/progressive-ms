// To add a drop shadow to menu on scroll > 50.

jQuery(document).ready(function($) {
    var div = $("#tranny-nav");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 50) {
            div.addClass("dropShadow");
        } else {
            div.removeClass("dropShadow");
        }
    });
});

