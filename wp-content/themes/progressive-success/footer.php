<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="brand-secondary-grad-bg contact-footer pb-5">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h2>Keep up to date</h2>
                <p class="lead">Get the latest property news and techniques with our exclusive Property Insider newsletter, direct to your inbox.</p>
            </div>
            <div class="col-6">
                <?php gravity_form(3, false, false, false, '', true, 12); ?>
            </div>
        </div>
    </div>
</div>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

                      &copy; 2018 <a class="inherit-color" href="<?php echo esc_url( home_url( '/' ) ); ?>"</a> <a href="http://getprogressive.co.uk/">Progressive Group</a><span class="sep"> | </span>Design: <a href="http://beprogressive.com">Progressive Digital</a>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>
</html>

