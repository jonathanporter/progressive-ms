<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="container-fluid" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

						<!-- understrap_post_nav(); REMOVED  -->

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->

		<!-- Do the right sidebar check -->


	</div><!-- .row -->
</div><!-- Container end -->

</div><!-- Wrapper end -->

<div class="positive-bg-row mt-5">
	<div class="container">
		<div class="row justPad">
			<div class="col-md-6">
				<h3>If you liked this post you may like these:</h3>
			</div>
			<div class="col-md-6">
<!-- 				<a href="#" class="btn btn-primary btn-lg float-right mt-0">Back to category</a>
 -->			</div>
		</div>
		<?php echo do_shortcode("[justified_image_grid thumbs_spacing=16 row_height=360 height_deviation=60 max_rows=1 aspect_ratio=1:1 disable_cropping=no developer_link=hide recent_posts=yes recents_description=auto_manual_excerpt recents_description_2=auto_manual_excerpt recents_include=blog]"); ?>
	</div>
</div>	
<div class="gray-lighter-bg-row mt-5 mask-group-bl pb-4">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2 class="brand-primary-colour">Get Your Free Report Now.</h2>
				<p class="lead">Get a FREE copy of our step-by-step quick-start guide "How To Make A Fortune From Buy-To-Let in 2019"</p>
				<p class="lead">(delivered to your email inbox instantly!)</p>
				<p class="lead">Just enter your details below</p>
			</div>
			<div class="col-md-2 d-flex flex-row align-items-center text-center">
				<i class="align-self-center icon-progressive-arrow-right-flush-x3 i5x brand-primary-colour mx-auto d-none d-sm-block"></i>
			</div>
			<div class="col-md-4">
				<img class="img-fluid" src="/wp-content/uploads/sites/2/2019/01/buy-to-let-property-report.png">
				<p class="lead d-block d-sm-none mt-5">Just enter your details below</p>
			</div>	
		</div>
	</div>
</div>
<?php get_footer(); ?>
