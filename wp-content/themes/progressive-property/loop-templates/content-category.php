<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<header class="entry-header negative tinted-image" style="
		background-image: linear-gradient(
		rgba(0, 128, 198, 1), 
		rgba(62, 47, 128, 1)
		), url('<?php echo $backgroundImg[0]; ?>');">

	<div class="container">
		<?php the_title( '<h1 data-aos="fade" class="entry-title aos-init aos-animate">', '</h1>' ); ?>

		<?php if( get_field('subheading') ): ?>
		    <h2 class="h5 aos-init aos-animate" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="400"><?php the_field('subheading'); ?></h2>
		<?php endif; ?>

		<hr class="plain">
		<!-- Breadcumbs -->
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
				<?php if(function_exists('bcn_display')) {
					bcn_display();
				}?>
			</div>
		<!-- END Breadcrumbs -->
	</div>

	</header><!-- .entry-header -->

	<!-- <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?> -->

	<div class="entry-content">

		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
