<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<header class="entry-header negative tinted-image" style="
		background-image: linear-gradient(
		rgba(0, 128, 198, 1), 
		rgba(62, 47, 128, 1)
		), url('<?php echo $backgroundImg[0]; ?>');">
		<!-- <div class="overlay"></div> -->
			<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<hr>
					<!-- Breadcumbs -->
					<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
						<?php if(function_exists('bcn_display')) {
							bcn_display();
						}?>
					</div>
					<!-- END Breadcrumbs -->
			</div>	
	</header><!-- .entry-header -->


<!-- 	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?> -->

	<div class="entry-content">
		<div class="entry-meta">
			<?php understrap_posted_on(); ?>
		</div><!-- .entry-meta -->
		
		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

<!-- .entry-footer REMOVED -->

</article><!-- #post-## -->
