<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}


if(function_exists('add_theme_support')) {
  add_theme_support('category-thumbnails');
}

add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );


//* TN Dequeue Styles - Remove Font Awesome from WordPress theme https://technumero.com/remove-font-awesome-from-wordpress-theme/
add_action( 'wp_print_styles', 'tn_dequeue_font_awesome_style' );
function tn_dequeue_font_awesome_style() {
      wp_dequeue_style( 'fontawesome' );
      wp_deregister_style( 'fontawesome' );
}

/**
 * Improve RSS feed to parent and other brands https://woorkup.com/show-featured-image-wordpress-rss-feed/#add-featured-image-rss-feed-code
 */
function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
  $content = '<div>' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'margin-bottom: 15px;' ) ) . '</div>' . $content;
}
  return $content;
}
 
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');


/**
 * AOS animation library https://github.com/michalsnik/aos
*/
function aos_js() {
    wp_enqueue_script( 'aos-js', get_stylesheet_directory_uri() . '/js/aos.js', NULL, NULL, true );
}
add_action( 'wp_enqueue_scripts', 'aos_js' );

function aos_init() {
  ?>
  <script>
    AOS.init();
    $('h1').attr('data-aos', 'fade-down'); // you may add more data attributes if required
    $('h2').attr('data-aos', 'zoom-in'); // you may add more data attributes if required
    $('i').attr('data-aos', 'zoom-in'); //
  </script>
  <?php
}


/* Add Contact Methods in the User Profile - https://codex.wordpress.org/Plugin_API/Filter_Reference/user_contactmethods */

function add_user_contact_methods( $user_contact ) {
 $user_contact['facebook'] = __( 'Facebook URL' );
 $user_contact['skype'] = __( 'Skype Username' );
 $user_contact['twitter'] = __( 'Twitter Handle' );
 $user_contact['youtube'] = __( 'Youtube Channel' );
 $user_contact['linkedin'] = __( 'LinkedIn' );
 $user_contact['pinterest'] = __( 'Pinterest' );
 $user_contact['instagram'] = __( 'Instagram' );
 $user_contact['github'] = __( 'Github profile' ); 
 return $user_contact; 
}
add_filter( 'user_contactmethods', 'add_user_contact_methods' );

// https://easywebdesigntutorials.com/create-your-own-author-bio-box-in-wordpress-without-a-plugin/

//Load Fontawesome
// function themeprefix_fontawesome_styles() {
//  wp_register_style( 'fontawesome' , 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', '' , '4.4.0', 'all' );
//  wp_enqueue_style( 'fontawesome' );
// }
// add_action( 'wp_enqueue_scripts', 'themeprefix_fontawesome_styles' ); 


function wpb_author_info_box( $content ) {

global $post;

// Detect if it is a single post with a post author
if ( is_singular( 'post' ) && isset( $post->post_author ) ) { // target blog only?

// Get author's display name - NB! changed display_name to first_name. Error in code.
$display_name = get_the_author_meta( 'first_name', $post->post_author );

// If display name is not available then use nickname as display name
if ( empty( $display_name ) )
$display_name = get_the_author_meta( 'nickname', $post->post_author );

// Get author's biographical information or description
$user_description = get_the_author_meta( 'user_description', $post->post_author );

// Get author's website URL 
$user_website = get_the_author_meta('url', $post->post_author);

// Get author's email
$user_email = get_the_author_meta('email', $post->post_author);

// Get author's Facebook
$user_facebook = get_the_author_meta('facebook', $post->post_author);

// Get author's Skype
$user_skype = get_the_author_meta('skype', $post->post_author);

// Get author's Twitter
$user_twitter = get_the_author_meta('twitter', $post->post_author);

// Get author's LinkedIn 
$user_linkedin = get_the_author_meta('linkedin', $post->post_author);
 
// Get author's Youtube
$user_youtube = get_the_author_meta('youtube', $post->post_author);

// Get author's Google+
$user_googleplus = get_the_author_meta('googleplus', $post->post_author);

// Get author's Pinterest
$user_pinterest = get_the_author_meta('pinterest', $post->post_author);

// Get author's Instagram
$user_instagram = get_the_author_meta('instagram', $post->post_author);
// Get author's Github
$user_github = get_the_author_meta('github', $post->post_author);


// Get link to the author archive page
$user_posts = get_author_posts_url( get_the_author_meta( 'ID' , $post->post_author));
if ( ! empty( $display_name ) )
$author_details = '<p class="col-md-9 offset-md-3 author_name">About ' . $display_name . '</p>';

// Author avatar - - the number 256 is the px size of the image.
$author_details .= '<div class="col-md-3 author_image tab-content">' . get_avatar( get_the_author_meta('ID') , 256 ) . '</div>';
$author_details .= '<p class="col-md-12 author_bio">' . get_the_author_meta( 'user_description' ). '</p>';
$author_details .= '<div class="author_links"><a class="btn btn-gray-lighter mr-3" href="'. $user_posts .'">About ' . $display_name . '</a><a class="btn btn-gray-lighter" href="'. $user_posts .'">All posts by ' . $display_name . '</a></p>'; 

// Display 

// Check if author has a website in their profile
if ( ! empty( $user_website ) ) {
// Display author website link
$author_details .= '<a href="' . $user_website .'" target="_blank" rel="nofollow" >Website</a></p>';
} else { 
// if there is no author website link then just close the paragraph
$author_details .= '</p>';
}


// Converted to PP icons // Fontawesome icons: https://fontawesome.io/icons/

// Display author Email link

/*
$author_details .= ' <a href="mailto:' . $user_email .'" target="_blank" rel="nofollow" title="E-mail" class="tooltip"><i class="icon-envelope i2x"></i> </a></p>';

// Check if author has Facebook in their profile
if ( ! empty( $user_facebook ) ) {
// Display author Facebook link
$author_details .= ' <a href="' . $user_facebook .'" target="_blank" rel="nofollow" title="Facebook" class="tooltip"><i class="icon-facebook i2x"></i> </a></p>';
} else { 
// if there is no author Facebook link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has Skype in their profile
if ( ! empty( $user_skype ) ) {
// Display author Skype link
$author_details .= ' <a href="' . $user_skype .'" target="_blank" rel="nofollow" title="Username Skype" class="tooltip"><i class="icon-skype i2x"></i> </a></p>';
} else { 
// if there is no author Skype link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has Twitter in their profile
if ( ! empty( $user_twitter ) ) {
// Display author Twitter link
$author_details .= ' <a href="' . $user_twitter .'" target="_blank" rel="nofollow" title="Twitter" class="tooltip"><i class="icon-twitter-square i2x"></i> </a></p>';
} else { 
// if there is no author Twitter link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has LinkedIn in their profile
if ( ! empty( $user_linkedin ) ) {
// Display author LinkedIn link
$author_details .= ' <a href="' . $user_linkedin .'" target="_blank" rel="nofollow" title="LinkedIn" class="tooltip"><i class="icon-linkedin-square i2x"></i> </a></p>';
} else { 
// if there is no author LinkedIn link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has Youtube in their profile
if ( ! empty( $user_youtube ) ) {
// Display author Youtube link
$author_details .= ' <a href="' . $user_youtube .'" target="_blank" rel="nofollow" title="Youtube" class="tooltip"><i class="icon-youtube-square i2x"></i> </a></p>';
} else { 
// if there is no author Youtube link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has Google+ in their profile
if ( ! empty( $user_googleplus ) ) {
// Display author Google + link
$author_details .= ' <a href="' . $user_googleplus .'" target="_blank" rel="nofollow" title="Google+" class="tooltip"><i class="icon-google-plus-square i2x"></i> </a></p>';
} else { 
// if there is no author Google+ link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has Pinterest in their profile
if ( ! empty( $user_pinterest ) ) {
// Display author Pinterest link
$author_details .= ' <a href="' . $user_pinterest .'" target="_blank" rel="nofollow" title="Pinterest" class="tooltip"><i class="icon-pinterest-square i2x"></i> </a></p>';
} else { 
// if there is no author Pinterest link then just close the paragraph
$author_details .= '</p>';
}

// Check if author has Github in their profile
if ( ! empty( $user_github ) ) {
// Display author Github link
$author_details .= ' <a href="' . $user_github .'" target="_blank" rel="nofollow" title="Github" class="tooltip"><i class="icon-git-square i2x"></i> </a></p>';
} else { 
// if there is no author Github link then just close the paragraph
$author_details .= '</p>';
}

*/

// Pass all this info to post content 
$content = $content . '</div><div class="gray-lighter-bg-row mt-5" id="author-row"> <div class="container"> <footer class="author_bio_section">' . $author_details . '<div class="author_bio_section"></div></div></footer> ';
}
return $content; 
}

// Add our function to the post content filter 
add_action( 'the_content', 'wpb_author_info_box' );

// Allow HTML in author bio section 
remove_filter('pre_user_description', 'wp_filter_kses');

?>


<?php

// https://gretathemes.com/guides/remove-category-title-category-pages/
function prefix_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'prefix_category_title' );



add_filter( 'woocommerce_page_title', 'woo_shop_page_title');

function woo_shop_page_title( $page_title ) {

    if( 'Shop' == $page_title) {
        return "Store";
    }
}







