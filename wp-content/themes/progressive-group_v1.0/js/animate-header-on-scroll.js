jQuery(document).ready(function($) {
    // Code that uses jQuery's $ can follow here.
var header = $('header');
var range = 2000;

$(window).on('scroll', function () {
  
  var scrollTop = $(this).scrollTop(),
      height = header.outerHeight(),
      offset = height / 2,
      calc = 1 - (scrollTop - offset + range) / range;

  header.css({ 'opacity': calc });

  if (calc > '1') {
    header.css({ 'opacity': 1 });
  } else if ( calc < '0' ) {
    header.css({ 'opacity': 0 });
  }
  
});
}

// jQuery(document).ready(function($) {
//     // Code that uses jQuery's $ can follow here.
// var range = 200;
//  var header = $('header');
// $(window).on('scroll', function () {
 
//     var scrollTop = $(this).scrollTop();
//     header.each(function(){
//     if($(this).position().top - scrollTop  <= 0){    
//               $(this).css({ 'opacity': 0 });      
//     }else{
//          $(this).css({ 'opacity': 1 });
//     }
//     });  
// }
