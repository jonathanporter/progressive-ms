<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );


//* TN Dequeue Styles - Remove Font Awesome from WordPress theme https://technumero.com/remove-font-awesome-from-wordpress-theme/
add_action( 'wp_print_styles', 'tn_dequeue_font_awesome_style' );
function tn_dequeue_font_awesome_style() {
      wp_dequeue_style( 'fontawesome' );
      wp_deregister_style( 'fontawesome' );
}

//  Dequeue Elementor Styles 
// add_action( 'wp_print_styles', 'dequeue_elementor_frontend_style' );
// function dequeue_elementor_frontend_style() {
//       wp_dequeue_style( 'elementor-frontend' );
//       wp_deregister_style( 'elementor-frontend' );
// }

/**
 * Improve RSS feed to parent and other brands https://woorkup.com/show-featured-image-wordpress-rss-feed/#add-featured-image-rss-feed-code
 */
function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
  $content = '<div>' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'margin-bottom: 15px;' ) ) . '</div>' . $content;
}
  return $content;
}
 
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');


/**
 * AOS animation library https://github.com/michalsnik/aos
 */
function nbm_aos_js() {
    wp_enqueue_script( 'aos-js', get_stylesheet_directory_uri() . '/js/aos.js', NULL, NULL, true );
}
add_action( 'wp_enqueue_scripts', 'nbm_aos_js' );

function nbm_aos_init() {
  ?>
  <script>
    AOS.init();
    $('h1').attr('data-aos', 'fade-down'); // you may add more data attributes if required
    $('h2').attr('data-aos', 'zoom-in'); // you may add more data attributes if required
    $('i').attr('data-aos', 'zoom-in'); //
  </script>
  <?php
}

// Gravity Form Mods on hold
// 5. Append custom CSS classes to the button
// add_filter( 'gform_submit_button', 'add_custom_css_classes', 10, 2 );
// function add_custom_css_classes( $button, $form ) {
//     $dom = new DOMDocument();
//     $dom->loadHTML( $button );
//     $input = $dom->getElementsByTagName( 'input' )->item(0);
//     $classes = $input->getAttribute( 'class' );
//     $classes .= " input-group-append another-one";
//     $input->setAttribute( 'class', $classes );
//     return $dom->saveHtml( $input );
// }


// Custom Hooks & Filters

  add_action( 'wp_head', 'wpcandy_actionhook_example' );

  function wpcandy_actionhook_example () {

    echo '<meta name="description" content="This is the meta description for this page." />' . "";

  } // End wpcandy_actionhook_example()

// Billing form




// Social sharing footer maybe?
//   add_filter( 'the_content', 'psocial_filterhook_signoff' );
//   function psocial_filterhook_signoff ( $content ) {
//     if ( is_single() ) {
//       $content .= '<p class="i3x d-flex flex-row justify-content-around p-5"><a href="//www.facebook.com/tantivy/" target="_blank"><i class="icon-facebook mr-3"></i></a><a href="//twitter.com/TantivyUK" target="_blank"><i class="icon-twitter mr-3"></i></a><a href="//www.linkedin.com/company/18280079/" target="_blank"><i class="icon-linkedin mr-3"></i></a></p>' . "
// ";
//     } // End IF Statement
//     return $content;
//   } // End wpcandy_filterhook_signoff()



add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
  $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
  $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    }
 
    return $title;
});