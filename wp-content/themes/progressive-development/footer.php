<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="brand-secondary-grad-bg contact-footer pb-5">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h3>Keep up to date</h3>
                <p>Get the latest property news and techniques with our exclusive Property Insider newsletter, direct to your inbox.</p>
            </div>
            <div class="col-6">
<!--                 <div class="input-group mb-3 mx-auto">
                    <input type="text" class="form-control" placeholder="youremail@domain.com" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <button class="btn btn-primary" id="basic-addon2">Submit</button>
                </div> -->

                <?php gravity_form(2, false, false, false, '', true, 12); ?>
            </div>
        </div>
    </div>
</div>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

                        &copy; 2018 <a class="inherit-color" href="<?php echo esc_url( home_url( '/' ) ); ?>"</a> <a href="http://getprogressive.co.uk/">Progressive Group</a><span class="sep"> | </span>Design: <a href="http://understrap.com">Progressive Digital</a>  (Version: 0.3.10)

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>
<!--  <script src="/wp-content/themes/progressive-property/js/aos.js"></script>
  <script>
    AOS.init();
  </script> -->
<script type="text/javascript">
	$('#carousel-col-3-courses').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $('.carousel-item').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});
</script>
</body>
</html>

